#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
========================
Active Appearance models
========================

This module implements active appearance models for use in audio-visual automatic speech recognition.

Author: Helge Reikeras

"""

import os, re
import numpy
from scipy import *
from scikits.image import io,opencv
from matplotlib.delaunay import delaunay
from scipy import ndimage
from itertools import permutations
from scipy.optimize import fmin_ncg

def Haffine_from_points(fp,tp):
    """ find H, affine transformation, such that 
        tp is affine transform of fp"""

    if fp.shape != tp.shape:
        raise RuntimeError, 'number of points do not match'

    #condition points
    #-from points-
    m = mean(fp[:2], axis=1)
    maxstd = max(std(fp[:2], axis=1))
    C1 = diag([1/maxstd, 1/maxstd, 1]) 
    C1[0][2] = -m[0]/maxstd
    C1[1][2] = -m[1]/maxstd
    fp_cond = dot(C1,fp)

    #-to points-
    m = mean(tp[:2], axis=1)
    C2 = C1.copy() #must use same scaling for both point sets
    C2[0][2] = -m[0]/maxstd
    C2[1][2] = -m[1]/maxstd
    tp_cond = dot(C2,tp)

    #conditioned points have mean zero, so translation is zero
    A = concatenate((fp_cond[:2],tp_cond[:2]), axis=0)
    U,S,V = linalg.svd(A.T)

    #create B and C matrices as Hartley-Zisserman (2:nd ed) p 130.
    tmp = V[:2].T
    B = tmp[:2]
    C = tmp[2:4]

    tmp2 = concatenate((dot(C,linalg.pinv(B)),zeros((2,1))), axis=1) 
    H = vstack((tmp2,[0,0,1]))

    #decondition
    H = dot(linalg.inv(C2),dot(H,C1))

    return H / H[2][2]

class AAM(object):
    def __init__(self, images=None, shapes=None, h5f=None):
        self.K = 0
        if h5f is not None:
            self.h5f = h5f
        # Build shape model
        self.build_shape_model(shapes)

        # Build texture model
        if images is not None:
            self.build_texture_model(images, shapes)
        
        # Perform pre-computation steps
        self.perform_pre_computation_steps()

    def align_shapes(self, s1, s2):
        """ Align shape s1 to s2 """

        # Translation

        x1 = s1[:,0]
        y1 = s1[:,1]

        x2 = s2[:,0]
        y2 = s2[:,1]
        
        offsetx1 = -mean(x1)
        offsety1 = -mean(y1)

        x1 = x1 + offsetx1
        y1 = y1 + offsety1

        offsetx2 = -mean(x2)
        offsety2 = -mean(y2)

        x2 = x2 + offsetx2
        y2 = y2 + offsety2

        # Scaling

        d1 = mean(sqrt(pow(x1,2)+pow(y1,2)))
        d2 = mean(sqrt(pow(x2,2)+pow(y2,2)))
        offsets = d2/d1

        x1 = x1*offsets
        y1 = y1*offsets

        # Rotation

        rot1 = arctan2(y1,x1)
        rot2 = arctan2(y2,x2)

        offsetr = -mean(rot1-rot2)
        rot1 = rot1+offsetr

        # Make the new points, which all have the same rotation
        dist = sqrt(pow(x1,2)+pow(y1,2))
        x1 = dist*cos(rot1)
        y1 = dist*sin(rot1)

        sa = empty(shape(s1))
        sa[:,0] = x1
        sa[:,1] = y1
        return sa

    def align_shapes2(self, S):
        N,K,D = shape(S)
        S = reshape(S,[N,K*D])
        m = mean(S,axis=1)
        
        for s in S:
            m = mean(s,axis=0)
            s -= m

        s1 = copy(S[0])
        s1 /= norm(s1)

        s0 = copy(s1)

        for i in range(100):

            for s in S:
                m = mean(s,axis=0)
                s -= m
                s /= dot(s,s1)
            s1 = mean(S,axis=0)
            s1 /= dot(s1,s0)
            s1 /= norm(s1)

        self.reference_shape = s1

        return S

    def build_shape_model(self,shapes,fv=0.98):
        """
        Contruct the shape model.
        """

        # Align shapes
        s0 = copy(shapes[0][0])
        self.reference_shape = s0

        # Align shapes
        X = []
        for i in range(len(shapes)):
            X.append([])
            for s in shapes[i]:
                X[-1].append(self.align_shapes(s,s0))

        self.mean_shape = mean(concatenate(X),axis=0)

        # Subtract mean shape for each speaker
        self.speaker_means = []
        for i in range(len(shapes)):
            self.speaker_means.append(mean(X[i],axis=0))
            X[i] -= self.speaker_means[i]


       # K: Number of samples, N: Number of points, D: Dimensionality

        X = concatenate(X)

        # Shape vectors will be stored (x1,y1,x2,y2,...,xn,yn). Thus
        # x = shapes[::2] and y = shapes[1::2]

        K,N,D = X.shape

        # Number of points
        self.N_points = N

        # Vectorize shapes
        NxD = N*D
        X = reshape(X,[K,NxD])

        self.x0bar = s0
        self.s0 = mean(X,axis=0)
        centered_shapes = X-self.s0

        w, v = numpy.linalg.eigh(cov(centered_shapes.T))
        # Eigenvalues are sorted from smallest to largest
        index = argsort(w)[::-1]
        w = w[index]

       # The normalized eigenvector corresponding to the eigenvalue w[i] is the column v[:,i].
        v = v[:,index]
        
        # eig_keep = greater(w[index],thresh)
        eig_keep = cumsum(w)<0.99*sum(w)

        K = sum(eig_keep)
        self.S = v[:,:K].T
        self.K += K

        self.s0_ = self.mean_shape

        self.S_ = empty([N,2,K])
        for k in range(K):
            self.S_[:,0,k] =  self.S[k][::2]
            self.S_[:,1,k] =  self.S[k][1::2]

        # Perform Delaunay Triangulation
        x0 = self.mean_shape[:,0]
        y0 = self.mean_shape[:,1]

        # Triangulate mean shape. Store result in a dictionary.
        self.tri_mesh = dict(zip(
            ['centers', 'edges', 'vertices', 'neighbors'],
            delaunay(x0,y0)
            ))
        self.tri_mesh['tri_N'] = len(self.tri_mesh['centers'])
        
    def build_texture_model(self, images, shapes, thresh=1e-4):
        """ Contruct the texture model """
        x0 = self.mean_shape[:,0]
        y0 = self.mean_shape[:,1]
        offsetx = -amin(x0)
        offsety = -amin(y0)
        x0 += offsetx
        y0 += offsety

        # Size of images
        images = asarray(images)
        self.image_size = images[0][0].shape

        N_s = self.N_points
        self.tp = vstack((x0,y0,ones((1,N_s))))

        # Raster scan triangulated mesh using Bresenham's Line Algorithm 

        import bresenham

        scan_lines = [[] for y in range(self.image_size[0])]
        for t,tri in zip(range(self.tri_mesh['tri_N']),self.tri_mesh['vertices']):
            v0,v1,v2 = around(self.s0_[tri])

            points = concatenate([bresenham.line(v0,v1), bresenham.line(v1,v2), bresenham.line(v2,v0)])
            y_min, y_max = min(points[:,1]), max(points[:,1])
            for y in range(y_min,y_max):
                points_in_line = points[find(points[:,1] == y)]
                scan_lines[y].append((t,min(points_in_line[:,0]),max(points_in_line[:,0])))

        self.tri_mesh['scan_lines'] = scan_lines 

        # Number of images
        MxN = prod(self.image_size)

        # Storage for vectorized images
        warped_images = []

        # convert points to homogenous coordinates
        tp = vstack((self.s0[:,1],self.s0[:,0],ones((1,N))))-vstack([amin_[:,None],0])

        template_shape = vstack([x0,y0]).T
        self.template_shape = template_shape
        images = self.h5f.getNode('/images')
        image_data = self.h5f.getNode('/images').cols
        NN = self.h5f.getNode('/images').nrows

        if self.h5f is None:
            for l in range(len(images)):
                warped_images.append([])
                for k in range(len(images[l])):
                    s = vstack([shapes[l][k][:,0],shapes[l][k][:,1]]).T
                    temp = zeros(self.image_size)
                    warped_images[-1].append(self.pw_affine(images[l][k],temp,s,temp_shape))

        else:
            # Get image data
            for i in range(NN):
                image_data.warped_image[i] = self.pw_affine(from_image=image_data.image[i],
                               to_image=image_data.warped_image[i],
                               fp=image_data.object_shape[i], 
                               tp=template_shape)

        # Compute speaker means
        self.speaker_mean_images = []
        for i in range(images.attrs.n_speakers):
            print i
            self.speaker_mean_images.append(mean(image_data.warped_image[:][image_data.id[:] == i],axis=0))
        # Subtract speaker mean
        for i in range(NN):
            image_data.warped_image[i] -= self.speaker_mean_images[image_data.id[i]]


        # Flatten images
        M,N = self.image_size

        # PCA
        mean_image = mean(image_data.warped_image[:],axis=0)

        feature_vectors = reshape(image_data.warped_image[:], [image_data.warped_image[:].shape[0], prod(image_data.warped_image[:].shape[1:])])

        for i in range(NN):
            image_data.vector_image[i] = reshape(image_data.warped_image[i], prod(image_data.warped_image[:].shape[1:]))
        print image_data.vector_image[:].shape
        w, v = numpy.linalg.eigh(cov(image_data.vector_image[:]))
        v = dot(image_data.vector_image[:].T,v)

        index = argsort(w)[::-1]
        w = w[index]
        v = v[:,index]

        # Keep only 98% of eigenvalues
        eig_keep = cumsum(w)<0.99*sum(w)

        K = 10
        #K = sum(eig_keep)
        w = w[eig_keep]
        v = v[:,:K]

        # Keep six appearance vectors
        self.v = v.T
        self.K += K
        self.A0, self.eig_images = reshape(mean_image,[M,N]), reshape(v.T,[K,M,N])

    def perform_pre_computation_steps(self):
        """ Precompute parameters """

        # 3) Compute gradient of A0
        self.grad_A0 = gradient(self.A0)
        self.grad_A0 = array([self.grad_A0[1],self.grad_A0[0]])
#        gradient(A0)[0] = dA0/dx, gradient(A0)[1] = dA0/dy

        N_p = len(self.S)
        N_s = len(self.s0_)

        # 4) Compute delta_W_wrt_p
        self.dWdp = zeros(concatenate([[2,N_p],self.image_size]))

        for y, line in zip(range(self.image_size[0]),self.tri_mesh['scan_lines']):
            for t,x1,x2 in line:
                x = arange(x1,x2)

                v_ind = self.tri_mesh['vertices'][t]
                
                for v_i in [v_ind, [v_ind[1],v_ind[0], v_ind[2]], [v_ind[2],v_ind[0],v_ind[1]]]:
                    xi0,yi0,xj0,yj0,xk0,yk0 = ravel(self.s0_[v_i])

                    denom = (xj0-xi0)*(yk0-yi0)-(yj0-yi0)*(xk0-xi0)
                    alpha = ((x-xi0)*(yk0-yi0)-(y-yi0)*(xk0-xi0))/denom
                    beta = ((y-yi0)*(xj0-xi0)-(x-xi0)*(yj0-yi0))/denom
                    self.dWdp[:,:,y,x.astype(int)] += (1-alpha-beta)*(self.S_[v_i[0]][:,:,None])


        # (5) Compute the steepest descent images (partial_W/parital_p) evaluated at (x;0)
        self.gradA0_dWdp = sum(self.grad_A0[:,None,:,:]*self.dWdp,axis=0)

        # (6) Compute Hessian using modifed steepest descent images
        self.H = zeros([N_p,N_p])
        
        for i in range(N_p):
            h1 = self.gradA0_dWdp[i]
            for j in range(N_p):
                h2 = self.gradA0_dWdp[j]
                self.H[j,i] = sum(h1*h2)

    def project(self,shape,image,speaker_id=None):
        # Feature vector
        f = concatenate([self.project_shape(shape,speaker_id),self.project_image(shape,image,speaker_id)])
        return f

    def project_shape(self,shape,speaker_id=None):
        # Feature vector
        x = self.align_shapes(shape,self.reference_shape)

        if speaker_id is not None:
            x = x - self.speaker_means[speaker_id]
        else:
            x = shape.copy()
        x = ravel(x)
        f_s = dot(self.S, (x-self.s0))
        return f_s

    def project_image(self,shape,image, speaker_id=None):
        # Feature vector
        s = vstack([shape[:,0],shape[:,1]]).T
        temp = zeros(self.image_size)
        warped_image = self.pw_affine(image,temp,s,self.template_shape)
        if speaker_id is not None:
            warped_image -= self.speaker_mean_images[speaker_id]
        f_v = dot(self.v, ravel(warped_image-self.A0))
        return f_v

    def fit_to_image(self, image, p_init = None, init_shape=None, test_shape=None):
        """ Fit AAM to a single image """

        # Create local references 

        N_s = self.N_points  # Number of vertices in triangulated mesh
        N_p = self.S_.shape[-1]  # Number of eigenvectors

        # Initialize p
        # TODO: Adapt to global shape transform
        if init_shape is None:
            p = zeros(len(self.S))
        else:
            s = ravel(init_shape)
            p = dot(self.S,(s-self.s0))

        s_old = copy(self.s0) # DEBUG
        p_old = copy(p)
        obj = inf

        ### ITERATE UNTIL CONVERGED ###

        k=0
        while sum(s - s_old)/N_s > 1e-3:
            k+=1
            # 1) Warp I with W(X;p) to compute I(W(x;p))

            s = ravel(self.s0 + dot(p[None,:],self.S))
            s_ = reshape(s,[N_s,2])

            x = s[::2]
            y = s[1::2]

            to_image = zeros(self.image_size)

            # 2) Compute error image

            warped_image = self.pw_affine(image,to_image,s_,self.s0_)

            error_image = warped_image-self.A0

            # Normalize 
            A0 = 256*(self.A0/amax(self.A0))
            warped_image = 256*(warped_image/amax(warped_image))
            error_image = warped_image-A0

            # (7) Compute sum_x[nabla_A0 x dW/dp][I(W(w;p))-A0(x)]
            b = empty(N_p)

            for i in range(N_p):
                b[i] = sum(self.gradA0_dWdp[i]*error_image)
    
            # (8) Compute delta_p
            delta_p = solve(self.H,b)

            # (9) Update the warp W(x;p) <- W(x;p) o W(x;delta_p)^-1
            delta_s0 = -ravel(dot(delta_p[None,:],self.S))

            x0 = self.s0[::2]
            y0 = self.s0[1::2]
            delta_x0 = delta_s0[::2]
            delta_y0 = delta_s0[1::2]

            tp = vstack([x,y,ones(N_s)])
            fp = vstack((x0,y0,ones([1,N_s])))
            fp2 = vstack((x0,y0,0.5*ones([1,N_s])))
            delta_fp = vstack((delta_x0,delta_y0,0.5*ones([1,N_s])))
 
            s_delta_s = zeros([N_s,2])
            for i in range(N_s):
                ii = 0
                for tt in self.tri_mesh['vertices']:
                    if i in tt:
                        ii += 1
                        H = Haffine_from_points(fp[:,tt],tp[:,tt])
                        s_delta_s[i] += dot(H,fp2[:,i]+delta_fp[:,i])[:2]
                s_delta_s[i] /= ii

            s_delta_s = ravel(s_delta_s)

            lhs = s_delta_s-self.s0
            
            # Update p (p=dot(tranpose(S),s_delta_s-self.s0))
            p_old  = copy(p)
            p = dot(self.S, lhs)

            # Compute difference
            to_image2 = zeros(self.image_size)
            s0_delta_s0 = ravel(self.s0 + dot(delta_p[None,:],self.S))
            s0_delta_s0_ = reshape(s0_delta_s0,[N_s,2])
            imshow(to_image)
            warped_A0 = self.pw_affine(self.A0,to_image2,self.s0_,s0_delta_s0_)

            obj_old = obj
            obj = sum(numpy.power((warped_image-warped_A0),2))

    def pw_affine(self, from_image, to_image, fp, tp):
        """ Piecewise affine image warp """
        tri_N = self.tri_mesh['tri_N']

        # Convert to homogenous coordinatges
        tp_h = vstack((tp[:,0],tp[:,1],ones((1,len(tp)))))
        fp_h = vstack((fp[:,0],fp[:,1],ones((1,len(fp)))))

        # Affine homography
        H_A = empty([tri_N,3,3])
        # Compute (a1,a2,a3,a4,a5,a6) for each triangle
        for t in range(self.tri_mesh['tri_N']):
            tri = self.tri_mesh['vertices'][t]
            H_A[t] = Haffine_from_points(tp_h[:,tri],fp_h[:,tri])

        # Perform warp TODO: interpolate
        for y0, line in zip(range(self.image_size[0]),self.tri_mesh['scan_lines']):
            for t,x1,x2 in line:
                x0 = arange(x1,x2)
                v = vstack([x0,tile(y0,len(x0)),ones(len(x0))])
                x,y,z = dot(H_A[t],v)
                to_image[y0, x0.astype(int)] = from_image[y.astype(int),x.astype(int)]

        return to_image

if __name__ == "__main__":
    image_dir = "./images"
    shape_dir = "./images"

    # Load images
    images = []
    shapes = []

    for filename in os.listdir(image_dir):
        base,ext = os.path.splitext(filename)
        if ext == '.jpg':
            images.append(io.imread(os.path.join(image_dir,filename), as_grey=True))
            shapes.append(loadtxt(os.path.join(shape_dir,'%s.pts'%base)))

    images = asarray(images)
    shapes = asarray(shapes)

    aam = AAM(images,shapes)

    test_image = io.imread("images/107_0764.jpg", as_grey=True)
    test_shape = loadtxt("images/107_0764.pts")
    aam.fit_to_image(test_image, init_shape=test_shape)
